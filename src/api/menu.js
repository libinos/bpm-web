import request from '@/utils/request'

export function getMenuTree(data) {
  return request({
    url: '/bpm/menu/getMenuTree/' + data,
    method: 'post'
  })
}

export function deleteMenu(data) {
  return request({
    url: '/bpm/menu/delete',
    method: 'post',
    data
  })
}

export function updateMenu(data) {
  return request({
    url: '/bpm/menu/update',
    method: 'post',
    data
  })
}

export function insertMenu(data) {
  return request({
    url: '/bpm/menu/insert',
    method: 'post',
    data
  })
}

export function getList(data) {
  return request({
    url: '/bpm/menu/getList',
    method: 'post',
    data
  })
}
